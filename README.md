## _Deprecation Notice_
This Pre-Built has been deprecated as of 09-01-2024 and will be end of life on 09-01-2025. The capabilities of this Pre-Built have been replaced by the [Versa - REST](https://gitlab.com/itentialopensource/pre-built-automations/versa-rest)

<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Versa Check Organization

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

This pre-built is used to check if the organization name provided is present in the Versa Director.
<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--

-->
<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 1 minute

## Requirements

This Pre-Built requires the following:

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Itential Automation Platform
  * `^2022.1`
* A running instance of the Itential OpenSource Versa Director adapter, which can be installed from [here](https://gitlab.com/itentialopensource/adapters/controller-orchestrator/adapter-versa_director).

## Features

The main benefits and features of the Pre-Built are outlined below.

Every Itential Pre-built is designed to optimize network performance and configuration by focusing on reliability, flexibility and coordination of network changes with applications and IT processes. As the network evolves, Pre-builts allow customers to focus on effective management and compliance to minimize risks of disruption and outage that can negatively impact quality of service.

<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## How to Install

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:

The pre-built can be run as part of a parent job by including or calling the workflow with fewer changes. 

The required values to be passed on to the pre-built are:
- Adapter ID (The id from the configuration of the Versa Adapter instance in IAP)
- Organization Name (The name of the organization to be checked in the Versa Director)

**Tasks**

- The getOrganizations task takes the adapter id and gets the details of all the organizations in the Versa director package. After further querying the names of the organizations from the response as shown below, the workflow then checks if the provided organization name input is present in the list of organization names obtained from the getOrganizations task and returns a boolean value.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
